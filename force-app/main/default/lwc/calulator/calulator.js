import { LightningElement, track } from 'lwc';
/* eslint-disable no-console */
/* eslint-disable no-alert */

export default class Calulator extends LightningElement {
    @track amount = 0;
    @track rate = 1;
    @track period = 365;


    handleChange(event) {
        const field = event.target.name;
        console.log(field);
        console.log(JSON.stringify(event.target));
        console.log(typeof event.target.value);
        if (field === 'periodInput') {
            this.period = parseInt(event.target.value, 10);
        }
        if (field === 'rateInput') {
            this.rate = parseFloat(event.target.value, 10);
        }
        if (field === 'amountInput') {
            this.amount = parseFloat(event.target.value, 10);
        }
    }

    get total() {
        return this.amount + (this.amount * this.period * this.rate / 365);
    }


}